<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "items".
 *
 * @property integer $id
 * @property string $item_name
 * @property string $dop_info
 * @property string $dop_info2
 * @property string $dop_info3
 */
class Item extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'items';
    }

    /**
     * Configure return fields
     * @return array
     */
    public function fields()
    {
        return ['id', 'item_name'];
    }

    /**
     * Configure extra fields
     * @return array
     */
    public function extraFields()
    {
        return ['dop_info', 'dop_info2', 'dop_info3'];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dop_info', 'dop_info2', 'dop_info3'], 'string'],
            [['item_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'item_name' => 'Item Name',
            'dop_info' => 'Dop Info',
            'dop_info2' => 'Dop Info2',
            'dop_info3' => 'Dop Info3',
        ];
    }
}
