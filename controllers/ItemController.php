<?php

namespace app\controllers;

use app\models\Item;
use yii\rest\Controller;
use yii\data\ActiveDataProvider;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;

/**
 * Class ItemController
 * @package app\controllers
 */
class ItemController extends Controller
{

    /**
     * @return mixed
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                QueryParamAuth::className(),
                HttpBearerAuth::className(),
            ],
        ];
        return $behaviors;
    }


    /**
     * @return mixed
     */
    public function actionIndex($id = null, $itemName = null)
    {
        $items = Item::find();

        if (!is_null($id)) {

            if (!is_numeric($id)) {
                
                throw new BadRequestHttpException('Invalid parameter id');
            }
        }

        if (!is_null($itemName)) {

            $items->where(['item_name' => $itemName]);
        }

        return new ActiveDataProvider([
            'query' => $items,
        ]);
    }

}
