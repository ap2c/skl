<?php

use yii\db\Migration;

class m170415_212834_add_data extends Migration
{
    public function up()
    {
        $columns = ['item_name', 'dop_info', 'dop_info2', 'dop_info3'];

        $rows = array();

        for ($x = 0; $x++ < 5;) {
            $rows[] = ['Name' . $x, 'info-' . $x, 'test-info', md5($x)];
        }

        for ($x = 5; $x++ < 10;) {
            $rows[] = ['Name5', 'info-' . $x, 'test-info', md5($x)];
        }

        $this->batchInsert ('items', $columns, $rows );
    }

    public function down()
    {
        $this->truncateTable('items');
    }
}
