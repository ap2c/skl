<?php

use yii\db\Migration;

/**
 * Handles the creation of table `items`.
 */
class m170415_212646_create_items_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('items', [
            'id' => $this->primaryKey(),
            'item_name' => $this->string(),
            'dop_info' => $this->text(),
            'dop_info2' => $this->text(),
            'dop_info3' => $this->text(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('items');
    }
}
