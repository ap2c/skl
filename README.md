SKL REST API 1.0
============================

API предоставляет следующие возможности:

2 способа авторизации:
======================

- По токену в строке запроса. Например:
~~~
http://skl2.ddns.net/item?access-token=dff8640a-f10f-4693
~~~
- Bearer: необходимо передать токен в заголовке запроса
~~~
Authorization: Bearer dff8640a-f10f-4693
~~~

Изменение формата ответа
========================

Формат ответа меняется с помощью заголовка:
- xml:  
~~~
accept: application/xml
~~~
- json: 
~~~
accept: application/json
~~~

либо с помощью GET параметра  _format:
- xml: 
~~~
_format=xml
~~~
- json: 
~~~
_format=json
~~~

Получение значений скрытых полей
================================

С помощью параметра **expand** можно получить значения скрытых полей: 'dop_info', 'dop_info2', 'dop_info3'
~~~
expand=dop_info,dop_info2
~~~


Примеры
=======

####Запрос
~~~
http://skl2.ddns.net/item?access-token=8f14e45fceea16&itemName=Name2
-------------------
Accept: application/xml
Accept-Encoding: gzip, deflate
~~~

####Ответ
~~~
HTTP/1.1 200 OK
Date: Sat, 15 Apr 2017 12:03:31 GMT
Server: Apache
Link: <http://skl2.ddns.net/item/index?access-token=8f14e45fceea16>; rel=self
Content-Length: 139
Keep-Alive: timeout=5, max=100
Connection: Keep-Alive
Content-Type: application/xml; charset=UTF-8
--------------------------------
<response>
    <item>
        <id>2</id>
        <item_name>Name2</item_name>
    </item>
</response>
~~~

####Запрос
~~~
http://skl2.ddns.net/item?expand=dop_info
-------------------
Accept: application/xml
Accept-Encoding: gzip, deflate
Authorization: Bearer dff8640a-f10f-4693-b372-966d54dfg
~~~

####Ответ
~~~
HTTP/1.1 200 OK
Date: Sat, 15 Apr 2017 12:03:31 GMT
Server: Apache
Link: <http://skl2.ddns.net/item/index?expand=dop_info>; rel=self
Content-Length: 139
Keep-Alive: timeout=5, max=100
Connection: Keep-Alive
Content-Type: application/xml; charset=UTF-8
--------------------------------
<response>
    <item>
        <id>1</id>
        <item_name>Name1</item_name>
        <dop_info>info-1</dop_info>
    </item>
    <item>
        <id>2</id>
        <item_name>Name2</item_name>
        <dop_info>info-2</dop_info>
    </item>
    ...
</response>
~~~

